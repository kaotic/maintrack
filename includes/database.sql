CREATE DATABASE IF NOT EXISTS `maintrack` DEFAULT CHARACTER SET latin1 DEFAULT COLLATE latin1_swedish_ci;
USE maintrack;

CREATE TABLE `maint` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `startDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer` varchar(128) NOT NULL,
  `impact` int(1) NOT NULL DEFAULT '0',
  `ticket` varchar(32) NOT NULL,
  `vendorTicket` varchar(32) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Change passowrd to something mroe secure
-- TODO: Create install script to handle this.
CREATE USER 'MT'@'localhost' IDENTIFIED BY '12345';
GRANT ALL PRIVILEGES ON maintrack . * TO 'MT'@'localhost';
FLUSH PRIVILEGES;
