## MainTrack

A simple maintenance tracking application, maintenance events are submitted via add.php. All events that start within 2 weeks will be displaed on the index page.

This code is still very much in BETA form. Please submit any and all bugs found.

### Installation

Import includes/database.sql into your database, Be sure to update the password to something more secure on line 18.

copy includes/dbconnect.sample to includes/dbconnect.php and update the database values to point to your database.

Run composer install on the root of the project to install the dependancies.

### Timezone support
The application supports multiple timezoens, ANY [PHP supported timezone](http://php.net/manual/en/timezones.php) can be passed to the URL by visiting http://example.com/America/Los_Angeles

The add event page generates a list of all Timezones in the Americas and Pacific, This is configurable in includes/functions.php under the generate_timezone_list() function.

    static $regions = array(
        DateTimeZone::AMERICA,
        DateTimeZone::PACIFIC,
    );

Please file any bugs at: https://gitlab.com/kaotic/maintrack/issues
