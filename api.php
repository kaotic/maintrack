<?php
require('includes/functions.php');
require_multi('includes/dbconnect.php', 'vendor/autoload.php');

if ($_POST && validateDate($_POST['startDate']) && validateDate($_POST['endDate']) && !empty($_POST['customer']) && !empty($_POST['ticket'])) {
  //define values to be saved to the database.
  $pdo = new PDO('mysql:host='.MYSERVER.';dbname='.MYDB, MYUSER, MYPASS);
  $stmt = $pdo->prepare("INSERT INTO maint (startDate, endDate, customer, ticket, vendorTicket, notes) VALUES (:startDate, :endDate, :customer, :ticket, :vendorTicket, :notes)");
  $stmt->bindParam(':startDate', $_POST['startDate'], PDO::PARAM_STR);
  $stmt->bindParam(':endDate', $_POST['endDate'], PDO::PARAM_STR);
  $stmt->bindParam(':customer', $_POST['customer'], PDO::PARAM_STR);
  $stmt->bindParam(':ticket', $_POST['ticket'], PDO::PARAM_STR);
  $stmt->bindParam(':vendorTicket', $_POST['vendorTicket'], PDO::PARAM_STR);
  $stmt->bindParam(':notes', $_POST['notes'], PDO::PARAM_STR);

  //Exicute SQL
  $stmt->execute();

  //fin
}

?>
