<?php
require('includes/functions.php');
require_multi('includes/dbconnect.php', 'vendor/autoload.php');
date_default_timezone_set('GMT');

//Define varius TwigVariables (tv) into an array for ease of passing to twig template.
$tv = array (
  'daysForward' => 6,
  'daysBack' => 2,
);

//Define Empty arrays incase the database does not return any data.
$upcomming = [];
$past = [];

try {
  $pdo = new PDO('mysql:host='.MYSERVER.';dbname='.MYDB, MYUSER, MYPASS);

  // Get upcomming maint events
  $result = $pdo->query("SELECT id, startDate, endDate, customer, impact, ticket, vendorTicket, description, notes FROM maint WHERE endDate BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL +6 DAY) ORDER BY startDate ASC, endDate ASC;");
  while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $upcomming[] = $row;
  }

  // Get past maint events
  $sql = "SELECT id, startDate, endDate, customer, impact, ticket, vendorTicket, description, notes FROM maint WHERE endDate BETWEEN DATE_ADD(NOW(), INTERVAL -:daysback DAY) AND NOW() ORDER BY endDate DESC;";
  $result = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
  $result->execute(array(':daysback' => $tv['daysBack']));

  while($row = $result->fetch(PDO::FETCH_ASSOC))

  {
    $past[] = $row;
  }
} catch (PDOException $e) {
  die("Error Connecting to the database.");
}

// determine refresh rate.
if (isset($_GET['r'])) {$tv['r'] = $_GET['r']; settype($r, "integer");} else { $tv['r'] = 90; settype($r, "integer"); }

if (isset($_GET['lt'])) {$tv['lt'] = true; settype($tv['lt'], "bool");} else { $tv['lt'] = false; settype($tv['lt'], "bool"); }

// determine timezone offset
if (in_array($_GET['t'], DateTimeZone::listIdentifiers())) {
  $tv['timezone'] = $_GET['t'];
  setcookie('tz', $tv['timezone'], time() + (86400 * 365), "/");
} elseif (isset($_COOKIE['tz'])) {
  $tv['timezone'] = $_COOKIE['tz'];
  $redirect = "http://" . $_SERVER['SERVER_NAME'] . "/" . $tv['timezone'];
  header("Location: $redirect");exit;
} else {
  $tv['timezone'] = 'GMT';
}

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader);
$twig->getExtension('core')->setTimezone('GMT');
$twig->addExtension(new Twig_Extensions_Extension_Date());

$template = $twig->loadTemplate('index.twig');

echo $template->render(array(
    'upcomming' => $upcomming,
    'past' => $past,
    //TwigVariables (tv)
    'tv' => $tv
));
?>
