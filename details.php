<?php
require('includes/functions.php');
require_multi('includes/dbconnect.php', 'vendor/autoload.php');

$pdo = new PDO('mysql:host='.MYSERVER.';dbname='.MYDB, MYUSER, MYPASS);
$result = $pdo->prepare("SELECT startDate, endDate, customer, impact, ticket, vendorTicket, notes FROM maint WHERE id = ?;");
$result->execute(array($_GET['id']));

$maint = $result->fetch(PDO::FETCH_ASSOC);

// var_dump($maint);exit;

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader);

$template = $twig->loadTemplate('details.twig');

echo $template->render(array(
    'maint' => $maint
));

?>
