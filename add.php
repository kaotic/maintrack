<?php
require('includes/functions.php');
require_multi('includes/dbconnect.php', 'vendor/autoload.php');
date_default_timezone_set('GMT');

$message = NULL;

if (isset($_COOKIE['tz'])) {
  $timezone = $_COOKIE['tz'];
} elseif (isset($_POST['tz'])) {
  $timezone = $_POST['tz'];
} else {
  $timezone = NULL;
}

if ($_POST
  && ( validateDate($_POST['startDate']) || validateDate($_POST['startDate'], 'Y-m-d\TH:i') )
  && ( validateDate($_POST['endDate']) || validateDate($_POST['endDate'], 'Y-m-d\TH:i') )
  && !empty($_POST['tz'])
  && !empty($_POST['customer'])
  && !empty($_POST['ticket'])
  && !empty($_POST['description'])) {
  //Pass Success to the template
  $message = "success";

  //Build DateTime objects.
  $startDate = new DateTime($_POST['startDate'], new DateTimeZone($_POST['tz']));
  $endDate = new DateTime($_POST['endDate'], new DateTimeZone($_POST['tz']));

  //Convert objects to GMT
  $startDate->setTimezone(new DateTimeZone('GMT'));
  $endDate->setTimezone(new DateTimeZone('GMT'));
  $sDate = $startDate->format('Y-m-d\TH:i');
  $eDate = $endDate->format('Y-m-d\TH:i');

  //define values to be saved to the database.
  $pdo = new PDO('mysql:host='.MYSERVER.';dbname='.MYDB, MYUSER, MYPASS);
  $stmt = $pdo->prepare("INSERT INTO maint (startDate, endDate, customer, ticket, vendorTicket, description, notes) VALUES (:startDate, :endDate, :customer, :ticket, :vendorTicket, :description, :notes)");
  $stmt->bindParam(':startDate', $sDate, PDO::PARAM_STR);
  $stmt->bindParam(':endDate', $eDate, PDO::PARAM_STR);
  $stmt->bindParam(':customer', $_POST['customer'], PDO::PARAM_STR);
  $stmt->bindParam(':ticket', $_POST['ticket'], PDO::PARAM_STR);
  $stmt->bindParam(':vendorTicket', $_POST['vendorTicket'], PDO::PARAM_STR);
  $stmt->bindParam(':description', $_POST['description'], PDO::PARAM_STR);
  $stmt->bindParam(':notes', $_POST['notes'], PDO::PARAM_STR);

  //Exicute SQL
  $stmt->execute();

  //fin
} /* Valadate dates */ elseif ($_POST && (!empty($_POST['startDate']) && !validateDate($_POST['startDate'])) || (!empty($_POST['endDate']) && !validateDate($_POST['endDate'])) ) {
  $message = "date";
} /* Valadate other Fields */ elseif ($_POST && (empty($_POST['customer']) || empty($_POST['ticket']) || empty($_POST['description'])) ) {
  $message = "other";
} /* Everything looks good? */ elseif (isset($message)) {

}

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader);

$template = $twig->loadTemplate('add.twig');

echo $template->render(array(
    'message' => $message,
    'self' => htmlspecialchars($_SERVER["PHP_SELF"]),
    'return' => htmlspecialchars($_SERVER["REQUEST_SCHEME"]) . "://" . htmlspecialchars($_SERVER["HTTP_HOST"]) . "/",
    'tz' => $timezone,
    'timezones' => generate_timezone_list()
));
?>
